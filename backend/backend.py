import logging
import psycopg2

# Configure logging
logging.basicConfig(filename='backend.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def connect_to_database():
    try:
        # Establish a connection to the database
        connection = psycopg2.connect(
            user="<username>",
            password="<password>",
            host="<host>",
            port="<port>",
            database="<database>"
        )
        logging.info("Connected to the database")
        return connection
    except (Exception, psycopg2.Error) as error:
        logging.error(f"Error connecting to
